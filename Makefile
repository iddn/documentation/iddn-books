all: build render

render:
	bin/render books/getting_started.yaml html/template.html > output/getting_started.html
	bin/render books/messages.yaml html/template.html > output/messages.html
	bin/render books/services.yaml html/template.html > output/services.html

build:
	go build -v -o bin/render src/render.go
