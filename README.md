# IDDN Books

IDDN Books in YAML Format

To build:
```
  go build -o bin/render src/render.go
```

To use:
```
  bin/render <book in yaml format> <html template>
```

